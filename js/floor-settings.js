var floorButtons = {
  'Mezzanine': [
    {
      top: '22%',
      left: '57%',
      image: './cgis/Mezzanine/1.jpg',
      title: 'TESTING - FOO', 
    },
    {
      top: '48%',
      left: '60%',
      image: './cgis/Mezzanine/2.jpg',
      title: 'TESTING - BAR', 
    },
  ],
  'Embankment': [
    {
      top: '25%',
      left: '60%',
      image: './img/80-STRAND-1020_023.jpg',
      title: 'TESTING2 - FOO', 
    },
    {
      top: 0,
      left: 0,
      image: '',
      title: 'TESTING2 - BAR', 
    },
  ],
  'Level 10': [
    {
      top: '10px',
      left: '10px',
      image: '',
      title: 'TESTING3 - FOO', 
    },
  ],
};

/**
 * Floor tooltips
 */
var floorTooltips = {
  'Mezzanine': [
    {
      top: '49%',
      left: '43.7%',
      title: 'Lift Lobby',
    },
    {
      top: '49.2%',
      left: '72.5%',
      title: 'Lift Lobby',
    },
  ],
  'Embankment': [
    {
      top: '49%',
      left: '43.7%',
      title: 'TEST FOO',
    },
    {
      top: '49.2%',
      left: '72.5%',
      title: 'TEST BAR',
    },
  ],
}