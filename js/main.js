// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    window.scrollTo({top: 0, behavior: 'smooth'});
}

$(function() {

    
    $( "#menu-trigger" ).on("click", function() {
        var isHidden = $("#main-navigation").is(":hidden");
        if (isHidden) {
            $('#header-text').hide();
            $('#header-logo').show();
            $('#main-navigation').slideDown(400);

            $('#menu-trigger').addClass('close');
        } else {
            $('#header-text').show();
            $('#header-logo').hide();
            $('#main-navigation').slideUp(200);

            $('#menu-trigger').removeClass('close');
        }
    });

    var waypoints = $('.menu-bg-change').waypoint(function(direction) {
        $('header').toggleClass('turquoise-bg');
    }, {
        offset: '70px'
    });






    // var waypoints = $('.cool-wrapper').waypoint(function(direction) {
    //     if ($('.cool-icon').hasClass('reset-cool')) {
    //         $('.cool-icon').animate({
    //             opacity: 0,
    //             left: "-300",
    //         });
    //         $('.cool-icon').toggleClass('reset-cool');
    //     } else {
    //         $('.cool-icon').animate({
    //             opacity: 1,
    //             left: "+=300",
    //         });
    //         $('.cool-icon').toggleClass('reset-cool');
    //     }
    // }, {
    //     offset: '10%'
    // });


    var waypoints = $('.choose-wrapper').waypoint(function(direction) {
        if ($('#level-choose').hasClass('reset-choose')) {
            $('#level-choose').animate({
                opacity: 0,
                right: "-300",
            });
            $('#level-choose').toggleClass('reset-choose');
        } else {
            $('#level-choose').animate({
                opacity: 1,
                right: "+=300",
            });
            $('#level-choose').toggleClass('reset-choose');
        }
    }, {
        offset: '50%'
    });



    var waypoints = $('.views-look-wrapper').waypoint(function(direction) {
        if ($('.views-look-graphic').hasClass('reset-look')) {
            $('.views-look-graphic').animate({
                opacity: 0,
                right: "-300",
            });
            $('.views-look-graphic').toggleClass('reset-look');
        } else {
            $('.views-look-graphic').animate({
                opacity: 1,
                right: "+=300",
            });
            $('.views-look-graphic').toggleClass('reset-look');
        }
    }, {
        offset: '50%'
    });


    // var waypoints = $('.location-image-overlay-wrapper').waypoint(function(direction) {
    //     if ($('.location-image-overlay').hasClass('reset-look')) {
    //         $('.location-image-overlay').animate({
    //             opacity: 0,
    //             left: "-300",
    //         });
    //         $('.location-image-overlay').toggleClass('reset-look');
    //     } else {
    //         $('.location-image-overlay').animate({
    //             opacity: 1,
    //             left: "+=300",
    //         });
    //         $('.location-image-overlay').toggleClass('reset-look');
    //     }
    // }, {
    //     offset: '50%'
    // });



    var waypoints = $('.snapshots-row-3').waypoint(function(direction) {
        if ($('.vaults-image').hasClass('reset-vault')) {
            $('.vaults-image').animate({
                top: "+=180",
            });
            $('.vaults-image').toggleClass('reset-vault');
        } else {
            $('.vaults-image').animate({
                top: "-180",
            });
            $('.vaults-image').toggleClass('reset-vault');
        }
    }, {
        offset: '50%'
    });


    var waypoints = $('#top-block').waypoint(function(direction) {
        if ($('.page-heading').hasClass('reset')) {
            $('.page-heading').animate({
                top: "-50",
                opacity: 0,
            }, {
                duration: 2000,
                specialEasing: {
                    width: "linear",
                    height: "easeOutBounce"
                }
            });
            $('.page-heading').toggleClass('reset');
        } else {
            $('.page-heading').animate({
                top: "+=50",
                opacity: 1,
            }, {
                duration: 2000,
                specialEasing: {
                    width: "linear",
                    height: "easeOutBounce"
                }
            });
            $('.page-heading').toggleClass('reset');
        }
    }, {
        offset: '100%',
    });



    var waypoints = $('.wellconnected').waypoint(function(direction) {
        // if ($('.wellconnected-heading').hasClass('reset')) {
        //     $('.wellconnected-heading').animate({
        //         top: "-30",
        //     }, {
        //         duration: 500,
        //         specialEasing: {
        //             height: "easeOutBounce"
        //         }
        //     });
        //     $('.wellconnected-heading').toggleClass('reset');
        // } else {
        //     $('.wellconnected-heading').animate({
        //         top: "+=30",
        //     }, {
        //         duration: 500,
        //         specialEasing: {
        //             height: "easeOutBounce"
        //         }
        //     });
        //     $('.wellconnected-heading').toggleClass('reset');
        // }
    }, {
        offset: '40%',
    });



    // var waypoints = $('.wellconnected-container-left').waypoint(function(direction) {
    //     if ($('.wellconnected-details').hasClass('reset')) {
    //         $('.wellconnected-details').animate({
    //             top: "-30",
    //         }, {
    //             duration: 500,
    //             specialEasing: {
    //                 height: "easeOutBounce"
    //             }
    //         });
    //         $('.wellconnected-details').toggleClass('reset');
    //     } else {
    //         $('.wellconnected-details').animate({
    //             top: "+=30",
    //         }, {
    //             duration: 500,
    //             specialEasing: {
    //                 height: "easeOutBounce"
    //             }
    //         });
    //         $('.wellconnected-details').toggleClass('reset');
    //     }
    // }, {
    //     offset: '10%',
    // });



    var waypoints = $('.location-pictures').waypoint(function(direction) {
        if ($('.cultural-hotspot').hasClass('reset')) {
            $('.cultural-hotspot').animate({
                top: "-30",
            }, {
                duration: 500,
                specialEasing: {
                    height: "easeOutBounce"
                }
            });
            $('.cultural-hotspot').toggleClass('reset');
        } else {
            $('.cultural-hotspot').animate({
                top: "+=30",
            }, {
                duration: 500,
                specialEasing: {
                    height: "easeOutBounce"
                }
            });
            $('.cultural-hotspot').toggleClass('reset');
        }
    }, {
        offset: '50%',
    });

    var waypoints = $('.snapshots-row-4').waypoint(function(direction) {
        if ($('.elegant-lunch-text').hasClass('reset')) {
            $('.elegant-lunch-text').animate({
                top: "0",
            }, {
                duration: 2000,
                specialEasing: {
                    height: "easeOutBounce"
                }
            });
            $('.elegant-lunch-text').toggleClass('reset');
        } else {
            $('.elegant-lunch-text').animate({
                top: "+=80",
            }, {
                duration: 2000,
                specialEasing: {
                    height: "easeOutBounce"
                }
            });
            $('.elegant-lunch-text').toggleClass('reset');
        }
    }, {
        offset: '20%',
    });

    var waypoints = $('.snapshots-row-5').waypoint(function(direction) {
        if ($('.snapshots-row-5 .col-1').hasClass('reset')) {
            $('.snapshots-row-5 .col-1').animate({
                backgroundSize: "100%",
            }, {
                duration: 500
            });
            $('.snapshots-row-5 .col-1').toggleClass('reset');
        } else {
            $('.snapshots-row-5 .col-1').animate({
                backgroundSize: "120%",
            }, {
                duration: 500
            });
            $('.snapshots-row-5 .col-1').toggleClass('reset');
        }
    }, {
        offset: '30%',
    });

    var waypoints = $('.location-pictures').waypoint(function(direction) {
        if ($('.covent-garden-text').hasClass('reset')) {
            $('.covent-garden-text').animate({
                top: "0",
            }, {
                duration: 2000,
                specialEasing: {
                    height: "easeOutBounce"
                }
            });
            $('.covent-garden-text').toggleClass('reset');
        } else {
            $('.covent-garden-text').animate({
                top: "+=50",
            }, {
                duration: 2000,
                specialEasing: {
                    height: "easeOutBounce"
                }
            });
            $('.covent-garden-text').toggleClass('reset');
        }
    }, {
        offset: '30%',
    });





    var settings = {
        objSlideTrigger: '#trigger', // link button id
        objSlidePanel: '.panel' // slide div class or id
    }

    $(settings.objSlideTrigger).bind('click' , function() {
        //If the panel isn't out
        if(!$(settings.objSlidePanel).hasClass('out')) {
            slidePanelOut();
        } else if($(settings.objSlidePanel).hasClass('out')) {
            slidePanelIn();
        }
    });

    function slidePanelOut() {
        //Animate it to left
        $(settings.objSlidePanel).animate({
            'right' : '-67%'
        });
        //Add the out class
        $(settings.objSlidePanel).addClass('out');
    }
    function slidePanelIn() {
        //Otherwise, animate it back in
        $(settings.objSlidePanel).animate({
            'right' : '-89%'
        });
        //Remove the out class
        $(settings.objSlidePanel).removeClass('out');
    }



    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };
    $( "#map-accordion" ).accordion({
        collapsible: true,
        // active: "none",
        heightStyle: "content",
        icons: icons
    });

    // Activates address
    var activateName = function (name) {
        var $buttonAndCircle = $('[data-map-name="' + name + '"]');
        $('[data-map-name]').not($buttonAndCircle).removeClass('is-active');
        $buttonAndCircle.addClass('is-active');

        if (window.outerHeight < 500 || window.outerWidth < 700) {
            $([document.documentElement, document.body]).animate({
            scrollTop: $('[data-map-name="' + name + '"]').offset().top - 200
            }, 2000);
        }
    };
    // When user clicks a location in list or on map
    $('[data-map-name]').click(function() {
        activateName($(this).attr('data-map-name'));
    });

    $( ".walking-times-container" ).on("click", function() {
        $('.wellconnected-container-right').toggleClass('walking');
    });

    // var waypoints = $('.turquoise-full-width').waypoint({
    //     handler: function() {
    //         console.log('Hit midpoint of my context')
    //         // notify('Hit midpoint of my context')
    //     },
    //     context: '#overflow-scroll-offset',
    //     offset: '50%'
    // })



    // NEW PANORAMIC STUFF
    var docWidth = $('body').width(),
        slidesWidth = $('#imgs').width(),
        rangeX = slidesWidth - docWidth,
        $images = $('#imgs');

    $(window).on('resize', function() {
        var docWidth = $('body').width(),
        slidesWidth = $('#imgs').width(),
        rangeX = slidesWidth - docWidth;
    });
    $(document).on('mousemove', function(e) {
        var mouseX = e.pageX,
        percentMouse = mouseX * 100 / docWidth,
        offset = percentMouse / 100 * slidesWidth - percentMouse / 200 * docWidth;
        $images.css({
            '-webkit-transform': 'translate3d(' + -offset + 'px,0,0)',
            'transform': 'translate3d(' + -offset + 'px,0,0)'
        });
    });
    $(document).on('touchmove', function(e) {
        var mouseX = e.pageX,
        percentMouse = mouseX * 100 / docWidth,
        offset = percentMouse / 100 * slidesWidth - percentMouse / 200 * docWidth;
        $images.css({
            '-webkit-transform': 'translate3d(' + -offset + 'px,0,0)',
            'transform': 'translate3d(' + -offset + 'px,0,0)'
        });
    });



    var slidesWidth_2 = $('#imgs-2').width(),
        rangeX_2 = slidesWidth_2 - docWidth,
        $images_2 = $('#imgs-2');

    $(window).on('resize', function() {
        var docWidth = $('body').width(),
        slidesWidth_2 = $('#imgs-2').width(),
        rangeX_2 = slidesWidth_2 - docWidth;
    });
    $(document).on('mousemove', function(e) {
        var mouseX_2 = e.pageX,
        percentMouse_2 = mouseX_2 * 100 / docWidth,
        offset_2 = percentMouse_2 / 100 * slidesWidth_2 - percentMouse_2 / 200 * docWidth;
        $images_2.css({
            '-webkit-transform': 'translate3d(' + -offset_2 + 'px,0,0)',
            'transform': 'translate3d(' + -offset_2 + 'px,0,0)'
        });
    });
    $(document).on('touchmove', function(e) {
        var mouseX_2 = e.pageX,
        percentMouse_2 = mouseX_2 * 100 / docWidth,
        offset_2 = percentMouse_2 / 100 * slidesWidth_2 - percentMouse_2 / 200 * docWidth;
        $images_2.css({
            '-webkit-transform': 'translate3d(' + -offset_2 + 'px,0,0)',
            'transform': 'translate3d(' + -offset_2 + 'px,0,0)'
        });
    });





    // fade title on page load
    $('.new-panoramic-title').show();
    $('.new-panoramic-title').delay(4000);
    $('.new-panoramic-title').fadeOut("slow");
    // $('.panoramic-title').addClass('reset');
    // $(window).scroll(function (event) {
    //     var scroll = $(window).scrollTop();
    //     if (scroll == 0) {
    //         $('.new-panoramic-title').show();
    //         $('.new-panoramic-title').delay(6000);
    //         $('.new-panoramic-title').fadeOut("slow");
    //         $('.new-panoramic-title').addClass('reset');
    //     } else {
    //         $('.new-panoramic-title').removeClass('reset');
    //     }
    // });










    // OLD PANORAMIC STUFF
    // $('#pan01').mousemove(function( event ) {
    //     var pageWidth = $(window).width();
    //     var pageMiddle = pageWidth / 2;
    //     var cursorPosition = event.pageX;
    //     var backgroundPos = $("#pan01.panoramic").css('backgroundPosition').split(" ");
    //         var xPos = backgroundPos[0],
    //             yPos = backgroundPos[1];
    //     //console.log(cursorPosition + ' ' + xPos);
    //     if (cursorPosition > pageMiddle) {
    //         $("#pan01.panoramic").stop().animate({
    //             backgroundPosition: "0%",
    //         }, {
    //             duration: 2000
    //         });
    //     } else {
    //         $("#pan01.panoramic").stop().animate({
    //             backgroundPosition: "100%",
    //         }, {
    //             duration: 2000
    //         });
    //     }
    // });

    // $('#pan02').mousemove(function( event ) {
    //     var pageWidth = $(window).width();
    //     var pageMiddle = pageWidth / 2;
    //     var cursorPosition = event.pageX;
    //     var backgroundPos = $("#pan02.panoramic").css('backgroundPosition').split(" ");
    //         var xPos = backgroundPos[0],
    //             yPos = backgroundPos[1];
    //     //console.log(cursorPosition + ' ' + xPos);
    //     if (cursorPosition > pageMiddle) {
    //         $("#pan02.panoramic").stop().animate({
    //             backgroundPosition: "0%",
    //         }, {
    //             duration: 2000
    //         });
    //     } else {
    //         $("#pan02.panoramic").stop().animate({
    //             backgroundPosition: "100%",
    //         }, {
    //             duration: 2000
    //         });
    //     }
    // });

    $(".day").on("click", function() {
        $("#Night_view").removeClass('active');
        $("#Day_View").addClass('active');
        $('#pan01').removeClass('panoramic-night');
        $('#pan01').addClass('panoramic-day');
    });
    $(".night").on("click", function() {
        $("#Day_View").removeClass('active');
        $("#Night_view").addClass('active');
        $('#pan01').removeClass('panoramic-day');
        $('#pan01').addClass('panoramic-night');
    });

    // fade title and panoramic controls on page load
    $('.panoramic-title').show();
    $('.panoramic-title').delay(4000);
    $('.panoramic-title').fadeOut("slow");
    $('.panoramic-control').show();
    $('.panoramic-control').delay(4000);
    $('.panoramic-control').fadeOut("slow");

    // Animate new panoramic controls
    $('.new-panoramic-controls').delay(50).animate({
        left: '100'
    }, 1000, 'swing').delay(50).animate({
        left: '-100'
    }, 1000, 'swing').delay(50).animate({
        left: '10'
    }, 1000, 'swing');

    
    setTimeout(function() {
        $('.new-panoramic-container').hide();
    }, 5000);


    var waypoints = $('#pan02').waypoint(function(direction) {
        if ($('#pan02').hasClass('reset')) {
            $('#pan02').toggleClass('reset');
        } else {
            $('.panoramic-control').show();
            $('.new-panoramic-controls').delay(50).animate({
                left: '100'
            }, 1000, 'swing').delay(50).animate({
                left: '-100'
            }, 1000, 'swing').delay(50).animate({
                left: '10'
            }, 1000, 'swing');
            $('.panoramic-control').delay(4000);
            $('.panoramic-control').fadeOut("slow");
            $('#pan02').toggleClass('reset');
        }
    }, {
        offset: '20%',
    });


    // $('.panoramic-title').addClass('reset');
    // $(window).scroll(function (event) {
    //     var scroll = $(window).scrollTop();
    //     if (scroll == 0) {
    //         $('.panoramic-title').show();
    //         $('.panoramic-title').delay(6000);
    //         $('.panoramic-title').fadeOut("slow");
    //         $('.panoramic-title').addClass('reset');
    //     } else {
    //         $('.panoramic-title').removeClass('reset');
    //     }
    // });

    // $(window).scroll(function(){
    //     console.log('title offset top: ' + $('.new-panoramic-title').offset().top);
    //     console.log($('.new-panoramic-title').offset().top < $(this).height() + $(this).scrollTop());
    // });



    // alert('Yep');
    // if ($('.panoramic-title').hasClass('reset')) {
    //     $('.panoramic-title').fadeIn("slow");
    //     $('.panoramic-title').toggleClass('reset');
    //     // alert('nope');
    // } else {
    //     $('.panoramic-title').delay(4000);
    //     $('.panoramic-title').fadeOut("slow");
    //     $('.panoramic-title').toggleClass('reset');
    //     $('.panoramic-title').toggleClass('reset');
    //     // alert('Yep');
    // }






    // $('.pan-left-button').on("click", function() {
    //     // Get the id of this panoramic so we can have more than one per page!
    //     var pan_this = $(this).closest('div[id]').prop('id');

    //     $("#" + pan_this + ".panoramic").css("animation-play-state", "paused");
    //     $("#" + pan_this + ".panoramic").removeClass('pan-right');
    //     var backgroundPos = $("#" + pan_this + ".panoramic").css('backgroundPosition').split(" ");
    //     var xPos = backgroundPos[0],
    //         yPos = backgroundPos[1];
    //     // console.log(xPos);
    //     if ( $("#" + pan_this + ".panoramic").hasClass('pan-left') ) {
    //         $("#" + pan_this + ".panoramic").css("backgroundPosition", xPos + ' 0%');
    //     }
    //     $("#" + pan_this + ".panoramic").bind('oanimationend animationend webkitAnimationEnd', function() {
    //         $("#" + pan_this + ".panoramic").css("backgroundPosition", '0% 0%');
    //     });
    //     $("#" + pan_this + ".panoramic").css("animation-play-state", "running");
    //     $("#" + pan_this + ".panoramic").toggleClass('pan-left');
    // });

    // $('.pan-right-button').on("click", function() {
    //     // Get the id of this panoramic so we can have more than one per page!
    //     var pan_this = $(this).closest('div[id]').prop('id');

    //     $("#" + pan_this + ".panoramic").css("animation-play-state", "paused");
    //     $("#" + pan_this + ".panoramic").removeClass('pan-left');
    //     var backgroundPos = $("#" + pan_this + ".panoramic").css('backgroundPosition').split(" ");
    //     var xPos = backgroundPos[0],
    //         yPos = backgroundPos[1];
    //     // console.log(xPos);
    //     if ( $("#" + pan_this + ".panoramic").hasClass('pan-right') ) {
    //         $("#" + pan_this + ".panoramic").css("backgroundPosition", xPos + ' 0%');
    //     }
    //     $("#" + pan_this + ".panoramic").bind('oanimationend animationend webkitAnimationEnd', function() {
    //         $("#" + pan_this + ".panoramic").css("backgroundPosition", '100% 0%');
    //     });
    //     $("#" + pan_this + ".panoramic").css("animation-play-state", "running");
    //     $("#" + pan_this + ".panoramic").toggleClass('pan-right');
    // });

    $(".level").on("click", function() {
        var level = $(this).data('level');

        $(this).addClass('active');
        setLevel(level);
        setPrevious(level);
        setNext(level);
    });
    $(".level-row").on("click", function() {
        var level = $(this).attr('id');
        setLevel(level);
        setPrevious(level);
        setNext(level);
    });
    var clickedLeft = '';
    var clickedRight = '';
    $(".floor-plan-left").on("click", function() {
        clickedLeft = $(this).attr("data-level");
        setLevel(clickedLeft);
        setPrevious(clickedLeft);
        setNext(clickedLeft);
    });
    $(".floor-plan-right").on("click", function() {
        clickedRight = $(this).attr("data-level");
        setLevel(clickedRight);
        setPrevious(clickedRight);
        setNext(clickedRight);
    });
    $(".mezz-button").on("click", function() {
        img = $(this).attr("data-pic");
        $(".mezz-button").removeClass('active');
        $(this).addClass('active');
        $("#level-cgi").css('background-image', 'url(' + img + ')');
    });

    function setPrevious(selectedLevel) {
        var $previous = $("#" + selectedLevel);
        var prevlevel = $previous.next().attr('id');
        $(".floor-plan-left").attr("data-level",prevlevel);
    }
    function setNext(selectedLevel) {
        var $next = $("#" + selectedLevel);
        var nextlevel = $next.prev().attr('id');
        $(".floor-plan-right").attr("data-level",nextlevel);
    }
    function setLevel(level) {

        
        var title = $(".level-row#" + level).find('.level-name').text();
        var feet = $(".level-row#" + level).find('.level-feet').text();
        var metres = $(".level-row#" + level).find('.level-m').text();

        $(".levels-right").css('background-image', 'url(./img/AXO/' + level + '.svg)');

        $("#level-cgi").css('background-image', 'url(./img/' + level + '.jpg)');
        $(".floor-plan-centre").css('background-image', 'url(./img/' + level + '-floorplan.svg)');
        
        // add floor eye buttons
        var _floorButtons = floorButtons[title];
        console.log(_floorButtons);

        $('.plan-btn').remove();
        $('.plan-tooltip').remove();

        _floorButtons.forEach(function (e, index){
            var id = 'plan-btn-' + index;

            $(".floor-plan-centre").append('<div id="' + id + '" class="plan-btn" style="top: ' + e.top + ';left: ' + e.left + '"></div>');

            document.getElementById(id).addEventListener('click', () => {
                $(".gallery-image").remove();
                $(".gallery").append('<a class="gallery-image" href='+e.image+'><img src='+e.image+' alt="'+e.title+'" title="'+e.title+'" /></a>');
                var gallery = new SimpleLightbox('.gallery a', {});
                gallery.open();
                console.log(gallery.open());
            });
        });


        // add floor tooltips
        var _floorTooltips = floorTooltips[title];
        console.log("Tooltips: " + _floorTooltips);


        _floorTooltips.forEach((e, index) => {
            var id = 'tooltip-' + index;
            console.log(id);

            $(".floor-plan-centre").append('<div id="' + id + '" class="plan-tooltip" style="top: ' + e.top + ';left: ' + e.left + '"></div>');
            
            $('#' + id).click(() => {
                $('.plan-tooltip').removeClass('plan-tooltip--flicker');
                $('.plan-tooltip').removeClass('plan-tooltip--active');
                $('#' + id).toggleClass('plan-tooltip--flicker');
                $('#' + id).toggleClass('plan-tooltip--active');
                $('#' + id).attr('data-before',e.title);
            });

        });
        
        // var width = $('.floor-plan-center');
        // console.log("title: "+ title);
        $("#level-title").text(title);
        $(".floor-plan-title").text(title);
        $(".floor-plan-ft").text(feet);
        $(".floor-plan-m").text(metres);
        $(".floor-plan-wrapper").show();
        $(".level-row").removeClass('active');
        $(".level-row#" + level).addClass('active');
        $('.level').removeClass('active');
        $('.level[data-level = '+level+']').addClass('active');

        if (level == 'level-mezzanine') {
            $(".mezz-button").removeClass('active');
            $("#CAT_A_CGI_BUTTON").addClass('active');
            $('.mezzanine-buttons').show();
        } else {
            $('.mezzanine-buttons').hide();
        }

        if (level == 'level-strand') {
            $(".mezz-button").removeClass('active');
            $("#CORPORATE_CGI_BUTTON_STRAND").addClass('active');
            $('.strand-buttons').show();
        } else {
            $('.strand-buttons').hide();
        }

        var nextlevel = $(".level-row#" + level).prev().find('.level-name').text();
        var previouslevel = $(".level-row#" + level).next().find('.level-name').text();
        if (previouslevel == "Total") {
            $(".floor-plan-left").find('span').text("");
            $(".floor-plan-left").find('img').hide();
        } else {
            $(".floor-plan-left").find('span').text(previouslevel);
            $(".floor-plan-left").find('img').show();
        }
        if (nextlevel == "") {
            $(".floor-plan-right").find('span').text("");
            $(".floor-plan-right").find('img').hide();
        } else {
            $(".floor-plan-right").find('span').text(nextlevel);
            $(".floor-plan-right").find('img').show();
        }

    }
});
